#!/usr/bin/env python3

'''
Setup module
'''

from setuptools import setup, find_packages, Command

import os
import re

def delete_file(file_name):
    os.remove(file_name)

def delete_files():
    delete_file("/clustering_preprocessing/model/helloworld.py")
    delete_file("/clustering_preprocessing/invocation/helloworld.py")

def remove_helloworld_lines(file_name):
    with open(file_name, "r+") as f:
        s = f.read()
        regex = "\# ->helloworld\n.*\n.*\# <-helloworld\n"
        result = re.sub(regex, "", s)
        f.seek(0)
        f.write(result)
        f.truncate()

def clean_files():
    remove_helloworld_lines("model_trainer/apiv1.py")
    remove_helloworld_lines("model_trainer/core/core.py")

def remove_helloworld():
    print("Removing helloworld examples...")
    delete_files()  
    clean_files()

class InitCommand(Command):
    description="Initialize and Configure the template"

    user_options = [
        ("rm-helloworld", "c", "Remove helloworld"),
    ]

    def initialize_options(self):
        self.rm_helloworld = 0
    
    def finalize_options(self):
        if self.rm_helloworld is None:
            raise Exception("Parameter --rm-helloworld is missing")

    def run(self):
        if self.rm_helloworld == 1:
            remove_helloworld()

setup(
    name='clustering-preprocessing',
    description='store clustering for assortment optimization',
    url='https://pmoslehi@bitbucket.org/gdcloudsolutions/clustering-preprocessing.git',
    version='d0.0.1',
    python_requires='>=3.5',
    packages=find_packages(exclude=['tests']),
    install_requires=[
        'sagemaker_pyspark',
        'cchardet',
        'numpy',
        'pandas',
        'scipy',
        'sklearn',
        'annoy',
        'aiohttp',
        'aiohttp-swagger',
        'boto3',
        'botocore',
        'pyspark==2.4.5'
    ],
    extras_require={
        'dev': [
            'pylint',
            'pytest-cov',
            'bumpversion',
            'requests'
        ],
        'prod': [
            'gunicorn',
        ],
    },
    test_suite="tests",
    include_package_data=True,

    # Custom command
    entry_points={
        'distutils.commands': [
            'add_gcloud_auth = scripts.ssot.add_gcloud_auth:AddGCloudAuthCommand'
        ]
    },
    cmdclass={
        'remove': InitCommand
    },

    # Metadata
    author='Groupe Dynamite Inc',
    author_email='',
    license='Apache License 2.0',
    long_description=open('README.md').read(),
    keywords=[],
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',

        # Pick your license as you wish (should match 'license' above)
        'License :: OSI Approved :: Apache Software License',

        'Operating System :: OS Independent',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ]
)
