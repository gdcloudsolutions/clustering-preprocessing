######################################################################################################
# Variables
######################################################################################################
ENV=dev
PART=patch
SVC=ao_store_clustering
IMAGE_NAME=$(subst _,-,$(SVC))
S3_BUCKET=s3://brain-ao-local/store-clustering/
S3_FILES=./ao_store_clustering/resources/clusteringFiles.txt
export NAME=$(shell basename $(CURDIR))
export VERSION=d0.0.1
export BRANCH_NAME=$(ENV)
export MODEL_PATH=$(CURDIR)/$(SVC)/resources
export AWS_PROFILE=$(ENV)
export SPARK_LOCAL_IP=127.0.0.1
export JAVA_HOME=/Library/Java/JavaVirtualMachines/adoptopenjdk-8.jdk/Contents/Home


######################################################################################################
# Basic tasks
######################################################################################################
install/parisa:
	@pip install -e .[$(ENV)]

listFiles:
	aws s3 ls $(S3_BUCKET) | awk '{print $(NF)}' > $(S3_FILES)

install:
	@pip install -i https://pypi.api.dynamite.ca/dynamite/public .[$(ENV)]

model/train:
	@python train

model/serve:
	@python serve

sagemaker/push:
	./build_and_push.sh $(IMAGE_NAME)-local $(VERSION)

bumpversion:
	@bumpversion $(PART)

clean/scaffold:
	@python setup.py remove --rm-helloworld

init:
	@python scripts/init_service.py --name $(name) --desc "$(desc)"

add/gcloud/auth:
	@python setup.py add_gcloud_auth

######################################################################################################
# Tests tasks
######################################################################################################
test: lint unittest

unittest:
	@python setup.py test

coverage:
	@py.test --cov-report term --cov-report html --cov=$(SVC) tests/

lint:
	@pylint $(SVC)
	@pylint tests

######################################################################################################
# Docker tasks
######################################################################################################

## serve/local: docker run -p 8080:8080 geny-test serve

docker/start: docker/build docker/run

docker/build:
	docker build -t $(NAME):$(VERSION) -t $(NAME):latest --build-arg BRANCH_NAME=$(BRANCH_NAME) .

docker/run:
	docker run -it -p 80:5000 $(NAME):latest ${CMD}

######################################################################################################
# Google cloud : encrypt/decrypt files tasks
######################################################################################################
encrypt:
	@gcloud kms encrypt --plaintext-file=$(FILE) --ciphertext-file=$(FILE).enc --location=global --keyring=google-cloud-build-secrets --key=builder

decrypt:
	@gcloud kms decrypt --plaintext-file=$(FILE) --ciphertext-file=$(FILE).enc --location=global --keyring=google-cloud-build-secrets --key=builder

######################################################################################################
# External configuration task
######################################################################################################
cfg-omni:
	kubectl -n config-mgt-common get secret cfg-omni-$(ENV) --export -o yaml | sed 's/^  name: .*/  name: cfg-omni/' | kubectl -n $(NAME)-$(ENV) apply -f -

cfg-cloudsql:
	kubectl -n config-mgt-common get secret cfg-cloudsql-$(ENV) --export -o yaml | sed 's/^  name: .*/  name: cfg-cloudsql/' | kubectl -n $(NAME)-$(ENV) apply -f -

######################################################################################################
# Versioning tasks
######################################################################################################

bumpversion/patch:
	@bumpversion patch

bumpversion/minor:
	@bumpversion minor

bumpversion/major:
	@bumpversion major

bumpversion/release:
	@bumpversion release --current-version $(VERSION) --new-version $(subst d,p,$(VERSION))

bumpversion/dev:
	@bumpversion minor --current-version $(VERSION) --new-version $(subst p,d,$(VERSION)) && make bumpversion/minor

tag/push:
	@git tag $(VERSION) && git push origin $(VERSION)