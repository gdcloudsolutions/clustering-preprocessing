from .app import App

def run():
    app = App()
    app.run()