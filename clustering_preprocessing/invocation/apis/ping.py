from aiohttp import web

routes = web.RouteTableDef()

@routes.get('/ping')
async def ping(request):
    """
    ---
    description: Health check endpoint
    tags:
    - Health check
    produces:
    - application/json
    responses:
        "200":
            description: success
    """
    return web.json_response({'status' : True})
