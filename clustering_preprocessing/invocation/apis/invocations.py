from aiohttp import web
import logging
import json

# Optional dependency. Read the function
from .. import getCore

routes = web.RouteTableDef()

@routes.post('/invocations')
async def invocations(request):
    """
        ---
        description: 
        tags:
        - Invocation
        produces:
        - application/json
        responses:
            "200":
                description: success
        """

    try:
        hi_sentence = getCore().hello_world.say_hi()
        result = {
            'status': True,
            'result': hi_sentence
        }
        return web.json_response(result, status=200)
    except Exception as e:
        logging.exception('Something went wrong')
        return web.json_response({'status' : False, 'result': e}, status=500)

