"""Core module"""

from ..config import Config

from .hello_world import HelloWorld


class Core:
    def __init__(self, config=None):
        self.config = config if config else Config()
        self.hello_world = HelloWorld()
    