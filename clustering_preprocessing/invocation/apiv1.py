from .apis import ping
from .apis import invocations

def setup(api):
    api.add_routes(ping.routes)
    api.add_routes(invocations.routes)
    
    return api