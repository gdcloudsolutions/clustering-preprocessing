from sagemaker_pyspark import IAMRole
from sagemaker_pyspark.algorithms import KMeansSageMakerEstimator


iam_role = "arn:aws:iam::195767839826:role/BrainAOSagemakerRole"


class KmensClustering:
    def __init__(self, core):
        self.config = core.config
        self.spark = core.spark
        self.features = core.features


    def train(self):
        training_data = self.features.read_data("s3a://studiobox-local/athena-query-staging/00760c1e-8a96-4d04-b577-faf235bf5ac8.csv", "3")

        test_data = self.features.read_data("s3a://studiobox-local/athena-query-staging/00760c1e-8a96-4d04-b577-faf235bf5ac8.csv", "3")

        kmeans_estimator = KMeansSageMakerEstimator(
            trainingInstanceType="ml.m4.xlarge",
            trainingInstanceCount=1,
            endpointInstanceType="ml.m4.xlarge",
            endpointInitialInstanceCount=1,
            sagemakerRole=IAMRole(iam_role))

        kmeans_estimator.setK(10)
        kmeans_estimator.setFeatureDim(3)

        kmeans_model = kmeans_estimator.fit(training_data)

        transformed_data = kmeans_model.transform(test_data)
        transformed_data.show()