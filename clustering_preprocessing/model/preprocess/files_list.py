
class S3Files:
    def __init__(self, core):
        self.config = core.config
        self.spark = core.spark
        self.s3_files_list = '/Users/pmoslehi/PycharmProjects/clustering-preprocessing/clustering_preprocessing/resources/clusteringFiles.txt'
        self.s3_folder = 's3://brain-ao-local/store-clustering/'


    def __read_file(self):
        with open(self.s3_files_list, 'r') as s3_files:
            return s3_files.readlines()

    def list_files(self):
        lst_files = []
        file_rows = self.__read_file()
        for row_id in range(len(file_rows)):
            if row_id != 0:
                lst_files.append(self.s3_folder + file_rows[row_id].split(' ')[-1].strip('\n'))
        return lst_files

