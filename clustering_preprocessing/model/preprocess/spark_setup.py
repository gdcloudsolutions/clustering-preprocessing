import sagemaker_pyspark
from pyspark.sql import SparkSession
import botocore.session


session = botocore.session.get_session()
credentials = session.get_credentials()
iam_role = "arn:aws:iam::195767839826:role/BrainAOSagemakerRole"

class SparkSetup:
    def __init__(self, core):
        self.config = core.config

    def __load_spark(self):
        classpath = ":".join(sagemaker_pyspark.classpath_jars())
        return (
            SparkSession
                .builder
                .config("spark.driver.extraClassPath", classpath)
                .config('fs.s3a.access.key', credentials.access_key)
                .config('fs.s3a.secret.key', credentials.secret_key)
                .getOrCreate()
        )


    def create_session(self):
        return self.__load_spark()
