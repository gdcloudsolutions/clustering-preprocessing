import pyspark.sql.functions as f
from pyspark.sql.types import DoubleType
from functools import reduce


class Features:
    def __init__(self, core):
        self.config = core.config
        self.spark = core.spark
        self.s3_folder = 's3a://brain-ao-local/store-clustering/'

    def __get_unique_values(self, df, in_col):
        return df.select(f.collect_set(in_col).alias(in_col)).first()[in_col]

    def __alias_dummy_columns(self, df, prefix):
        return df.select([f.col(c).alias(prefix + '_' + c) for c in df.columns])

    def __get_dummies(self, df, input_col):
        lst_unique_values = self.__get_unique_values(df, input_col)
        pivoted = df.groupBy("StoreNum", input_col).pivot(input_col, lst_unique_values).agg(f.lit(1)).na.fill(0)
        pivoted_data = self.__alias_dummy_columns(pivoted, input_col)
        pivoted_data = pivoted_data.withColumnRenamed(input_col + '_' + "StoreNum", "StoreNum")
        pivoted_data = pivoted_data.withColumnRenamed(input_col + '_' + input_col, input_col)
        return pivoted_data

    def __join_red(self, left, right):
        return left.join(right, on='StoreNum', how='inner')

    def __read_data(self, s3_file):
        # return self.spark.read.format("libsvm").option("numFeatures", num_features).load(s3_file)
        df = self.spark.read.option("delimiter", ",").csv(s3_file, header=True)
        return df

    def __rename_columns(self, df, prefix):
        for old_col in df.columns:
            if old_col != 'F_ItemLocationCode':
                df = df.withColumnRenamed(old_col, prefix + '_' + old_col)
        return df

    def prepare_data_frames(self):
        # d_sales = self.__read_data(self.s3_folder + 'D_sales.csv')
        # g_sales = self.__read_data(self.s3_folder + 'G_sales.csv')
        # store_mall_ref = self.__read_data(self.s3_folder + 'store_mall_ref.csv')
        #
        # last_long = self.__read_data(self.s3_folder + 'store_lat_long.csv')
        # num_stores = self.__read_data(self.s3_folder + 'NumStoresinMall.csv')

        main_data = self.__read_data(self.s3_folder + 'Total_demo_final.csv')
        main_grg = main_data.filter(f.col('Banner') == 'Garage').drop('Banner').withColumnRenamed("Store number",
                                                                                                  "StoreNum")
        main_dyn = main_data.filter(f.col('Banner') == 'Dynamite').drop('Banner').withColumnRenamed("Store number",
                                                                                                    "StoreNum")
        store_g = self.__read_data(self.s3_folder + 'StoreRefG.csv')
        lst_columns = ['STORE CONCEPT', 'REG vs OUTLET', 'Country']
        lst_dummies_g = [store_g.select('StoreNum', 'store_tenure')]
        for col in lst_columns:
            lst_dummies_g.append(self.__get_dummies(store_g, col))
        store_g = reduce(self.__join_red, lst_dummies_g)

        lst_columns.append('Store_classification')
        store_d = self.__read_data(self.s3_folder + 'StoreRefD.csv')
        lst_dummies_d = [store_d.select('StoreNum', 'store_tenure')]
        for col in lst_columns:
            lst_dummies_d.append(self.__get_dummies(store_d, col))
        store_d = reduce(self.__join_red, lst_dummies_d)

        weather_data = self.__read_data(self.s3_folder + 'us_ca_weather_data.csv')
        cc_data = self.__read_data(self.s3_folder + 'StoreCCNum.csv').withColumnRenamed("IDEAL CC's", "IDEALCC")
        cc_data_d = cc_data.filter(f.col('Banner') == 'Dynamite').select('StoreNum', 'IDEALCC')
        cc_data_g = cc_data.filter(f.col('Banner') == 'Garage').select('StoreNum', 'IDEALCC')
        conversion_APT = self.__read_data(self.s3_folder + '2018 Conversion & APT.csv')
        traffic = self.__read_data(self.s3_folder + 'traffic and Cust count.csv')
        selling_footage = self.__read_data(self.s3_folder + 'SellingSquareFeet.csv')
        weather_by_store_g = self.manipulate_weather_data(weather_data, 'Garage').withColumnRenamed("Storenumber",
                                                                                                    "StoreNum")
        weather_by_store_d = self.manipulate_weather_data(weather_data, 'Dynamite').withColumnRenamed("Storenumber",
                                                                                                      "StoreNum")

        main_d = self.__join_data(main_dyn, weather_by_store_d, selling_footage, store_d, cc_data_d, conversion_APT,
                                  traffic, 'inner')
        main_g = self.__join_data(main_grg, weather_by_store_g, selling_footage, store_g, cc_data_g, conversion_APT,
                                  traffic, 'inner')

        # .select('PercFemalePop10m', 'MinDistComp', 'Perc_non-visible minority', 'PercFemalePop5m', 'PercFemalePop15m',
        #         'Tot_45-64', 'Garage_10m', 'MedianInc', 'md.StoreNum', 'Perc_Other_apparel', 'STORE CONCEPT', 'Conversion %',
        #         'TotPop_within5m', 'Universities_within_5m', ' Tot_HH_within10m ', 'Selling SQFT ', 'TotPop_within10m',
        #         'IDEALCC', 'Minhum', 'Dynamite_10m', 'Universities_within_10m', 'MaxDistComp', 'totalRain', ' Unique Cust ',
        #         'Female_pop_within10m', 'Dist_Montreal (miles)', 'Avghum', 'Popdensity_within10m', 'Female_pop_within15m',
        #         ' Urbanicity index ', 'Maxhum', 'NumofStores', 'MinTemp', 'Popdensity_within5m', 'Perc25-44', 'AvgRain',
        #         'Popdensity_within15m', 'Perc_Women_Girls', 'Tot_15-19', 'AvgSnow', 'MaxTemp', 'Universities_within_15m',
        #         'Tot_25-44', 'Perc45-64', 'Perc_visible minority', 'AvgWind', 'AvgDistanceComp', 'Perc20-24', 'Dynamite_15m',
        #         'TotPop_within15m', 'Garage_5m', ' Tot_HH_within5m ', 'REG vs OUTLET', 'Perc15-19', 'Female_pop_within5m',
        #         ' Tot_HH_within15m ', 'store_tenure', 'Total_comp_1m', ' Traffic Count ', 'Country', 'Dynamite_5m', 'APT',
        #         'Garage_15m', 'AvgTemp', 'Tot_20-24', 'totalSnow', ' Pop_MSA', 'Perc_Children', 'Perc_Men_Boys')

        # sales data
        dep_g_row = self.__read_data(self.s3_folder + 'G_DepartmentSales.csv')
        dep_g = self.__pivot_data(dep_g_row, 'F_DepartmentName', 'F_ItemLocationCode', 'perc_sales').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")

        dep_d_row = self.__read_data(self.s3_folder + 'D_DepartmentSales.csv')
        dep_d = self.__pivot_data(dep_d_row, 'F_DepartmentName', 'F_ItemLocationCode', 'perc_sales').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")

        price_g_row = self.__read_data(self.s3_folder + 'G_PricepointSales.csv')
        price_g = self.__pivot_data(price_g_row, 'Price_cat', 'F_ItemLocationCode', 'perc_sales').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")

        price_d_row = self.__read_data(self.s3_folder + 'D_PricepointSales.csv')
        price_d = self.__pivot_data(price_d_row, 'Price_cat', 'F_ItemLocationCode', 'perc_sales').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")

        silhouette_g_row = self.__read_data(self.s3_folder + 'G_SilhouetteSales.csv')
        silhouette_g = self.__pivot_data(silhouette_g_row, 'Silhouette', 'F_ItemLocationCode',
                                         'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        silhouette_d_row = self.__read_data(self.s3_folder + 'D_SilhouetteSales.csv')
        silhouette_d = self.__pivot_data(silhouette_d_row, 'Silhouette', 'F_ItemLocationCode',
                                         'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        material_g_row = self.__read_data(self.s3_folder + 'G_MaterialSales.csv')
        material_g = self.__pivot_data(material_g_row, 'Material', 'F_ItemLocationCode',
                                       'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        material_d_row = self.__read_data(self.s3_folder + 'D_MaterialSales.csv')
        material_d = self.__pivot_data(material_d_row, 'Material', 'F_ItemLocationCode',
                                       'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        body_length_g_row = self.__read_data(self.s3_folder + 'G_BodyLengthSales.csv')
        body_length_g = self.__pivot_data(body_length_g_row, 'BodyLength', 'F_ItemLocationCode',
                                          'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        body_length_d_row = self.__read_data(self.s3_folder + 'G_BodyLengthSales.csv')
        body_length_d = self.__pivot_data(body_length_d_row, 'BodyLength', 'F_ItemLocationCode',
                                          'perc_sales').withColumnRenamed("F_ItemLocationCode", "StoreNum")

        perc_discount_d = self.__read_data(self.s3_folder + 'D_Perc_discount.csv').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")
        perc_discount_g = self.__read_data(self.s3_folder + 'G_Perc_discount.csv').withColumnRenamed(
            "F_ItemLocationCode", "StoreNum")

        # main_g.printSchema()
        # dep_g.printSchema()
        # price_g.printSchema()
        # silhouette_g.printSchema()
        # material_g.printSchema()
        # body_length_g.printSchema()
        # perc_discount_g.printSchema()

        main_g = self.__join_data(main_g, dep_g, price_g, silhouette_g, material_g, body_length_g, perc_discount_g,
                                  'left_outer')
        main_d = self.__join_data(main_d, dep_d, price_d, silhouette_d, material_d, body_length_d, perc_discount_d,
                                  'left_outer')

        main_g.printSchema()

        # filter closed storenumbers
        main_d = main_d.filter(main_d.StoreNum != '249')
        main_d.printSchema()

    def __pivot_data(self, df, pivot_col, group_col, value_col):
        # lst_unique_values = df.select(f.collect_set(pivot_col).alias(pivot_col)).first()[pivot_col]
        df = df.groupBy(group_col).pivot(pivot_col).agg(f.first(value_col))
        return self.__rename_columns(df, 'first_' + pivot_col)

    def __join_data(self, main_df, weather_by_store, selling_footage, store, cc_data, conversion, traffic, how):
        df = main_df.join(weather_by_store, ['StoreNum'], how) \
            .join(selling_footage, ['StoreNum'], "left_outer") \
            .join(store, ['StoreNum'], "left_outer") \
            .join(cc_data, ['StoreNum'], "left_outer") \
            .join(conversion, ['StoreNum'], "left_outer") \
            .join(traffic, ['StoreNum'], "left_outer")
        return df

    def __join_sales_data(self, main_df, dep, price, silhouette, material, body_length, perc_discount, how):
        df_ = main_df.join(dep, ['StoreNum'], how) \
            .join(price, ['StoreNum'], "left_outer") \
            .join(silhouette, ['StoreNum'], "left_outer") \
            .join(material, ['StoreNum'], "left_outer") \
            .join(body_length, ['StoreNum'], "left_outer") \
            .join(perc_discount, ['StoreNum'], "left_outer")
        return df_

    def manipulate_weather_data(self, df_weather, input_banner):
        df_rest = df_weather.filter(f.col('Banner') == input_banner).groupby('Storenumber').agg(
            {"AverageDailyAirTemp": "mean",
             "MaxDailyAirTemp": "max",
             "MinDailyAirTemp": "min",
             "AverageWindSpeed": "mean",
             "SpecificHumidity_mean": "mean",
             "MaxDailySpecificHumidity": "max",
             "MinDailySpecificHumidity": "min",
             "DailyRainFall_mm": "mean",
             "DailySnowFall_mm": "mean"}) \
            .withColumnRenamed('avg(AverageDailyAirTemp)', 'AvgTemp') \
            .withColumnRenamed('max(MaxDailyAirTemp)', 'MaxTemp') \
            .withColumnRenamed('min(MinDailyAirTemp)', 'MinTemp') \
            .withColumnRenamed('min(MinDailySpecificHumidity)', 'Minhum') \
            .withColumnRenamed('avg(AverageWindSpeed)', 'AvgWind') \
            .withColumnRenamed('avg(SpecificHumidity_mean)', 'Avghum') \
            .withColumnRenamed('max(MaxDailySpecificHumidity)', 'Maxhum') \
            .withColumnRenamed('avg(DailyRainFall_mm)', 'AvgRain') \
            .withColumnRenamed('avg(DailySnowFall_mm)', 'AvgSnow')

        df_rain_snow = df_weather.filter(f.col('Banner') == input_banner).groupby('Storenumber').agg(
            {"DailyRainFall_mm": "sum",
             "DailySnowFall_mm": "sum"}) \
            .withColumnRenamed('sum(DailyRainFall_mm)', 'totalRain') \
            .withColumnRenamed('sum(DailySnowFall_mm)', 'totalSnow')
        df = df_rest.alias('a').join(df_rain_snow.alias('b'), f.col('b.Storenumber') == f.col('a.Storenumber')) \
            .select('a.Storenumber', 'AvgTemp', 'AvgRain', 'MaxTemp', 'MinTemp', 'AvgSnow', 'Minhum', 'AvgWind',
                    'Avghum',
                    'Maxhum', 'totalRain', 'totalSnow')
        return df

    def test(self, df_weather, input_banner):
        groupBy = ["Storenumber"]
        aggregate = ["DailyRainFall_mm", "DailySnowFall_mm"]
        funs = [f.mean, f.sum, f.max, f.min]
        exprs = [fn(f.col(c)) for fn in funs for c in aggregate]

        df = df_weather.filter(f.col('Banner') == input_banner).groupby(*groupBy).agg(*exprs)
        df.show()
