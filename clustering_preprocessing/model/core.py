"""Core module"""

from ..config import Config
from .preprocess import spark_setup
from .preprocess import features
from .train import store_clustering
from .preprocess import files_list


class Core:
    def __init__(self, config=None):
        self.config = config if config else Config()
        self.spark_setup = spark_setup.SparkSetup(self)
        self.spark = self.spark_setup.create_session()
        self.files_list = files_list.S3Files(self)
        self.features = features.Features(self)
        # self.kmeans = store_clustering.KmensClustering(self)


    def start(self):
        self.features.prepare_data_frames()









