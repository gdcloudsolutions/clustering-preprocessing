import logging
import os

class HelloWorld:
    def say_hi(self):
        print('hello world')
        model_path = f'{os.environ["MODEL_PATH"]}/test.txt'
        with open(model_path, 'w') as my_file:
            my_file.write('test')
