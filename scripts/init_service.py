import argparse
import re
import os
import subprocess

def execute(cmd):
    proc = subprocess.run(cmd)
    proc.check_returncode()

def get_scaffold_content(regex, setup_py):
    content = re.search(regex, setup_py)[1]
    return content.replace("$", "\\$")

def get_name_and_desc(file="setup.py"):
    try:
        with open(file, "r") as f:
            setup_py = f.read()
            name = get_scaffold_content("name='(.*)'", setup_py)
            desc = get_scaffold_content("description='(.*)'", setup_py)

            return (name, desc)
    except:
        raise Exception(f"Failed to get name and description from {file} !")

def replace_on(file_name, name, desc, previous_name, previous_desc):
    print("Replacing '{}' and '{}' to '{}' and '{}' on '{}'".format(previous_name, previous_desc, name, desc, file_name))
    with open(file_name, "r+") as f:
        result = f.read()
        result = re.sub(previous_name, name, result)
        result = re.sub(previous_desc, desc, result)
        f.seek(0)
        f.write(result)
        f.truncate()

def replace_on_files(file_names, service_name, service_desc, previous_service_name, previous_service_desc):
    for file_name in file_names:
        replace_on(file_name, service_name, service_desc, previous_service_name, previous_service_desc)

def replace_svc_in_file(file_name, svc):
    cwd = os.getcwd()
    file_to_update = os.path.join(cwd, file_name)
    with open(file_to_update, 'r+') as f:
        result = f.read().replace("$MODEL_NAME", svc)
        f.seek(0)
        f.write(result)
        f.truncate()

def main():
    parser = argparse.ArgumentParser(description='Process initialization scripts parameters')
    parser.add_argument('--name', type=str, required=True, help='Service name')
    parser.add_argument('--desc', type=str, required=True, help='Service description')
    args = parser.parse_args()
    service_name = args.name
    service_desc = args.desc

    cwd = os.getcwd()

    previous_service_name, previous_service_desc = \
            get_name_and_desc(file=os.path.join(cwd, "setup.py"))

    service_name_no_hyphen = re.sub('[-]','_', service_name)
    execute(['git', 'mv', 'model_name', service_name_no_hyphen])

    files_to_update = [
        os.path.join(cwd, "setup.py"),
        os.path.join(cwd, "cloudbuild.yaml"),
        os.path.join(cwd, f"{service_name_no_hyphen}/config/default.json"),
        os.path.join(cwd, "README.md")]
    replace_on_files(files_to_update, service_name, service_desc, previous_service_name, previous_service_desc)

    
    replace_svc_in_file('Makefile', service_name_no_hyphen) 
    replace_svc_in_file('Dockerfile', service_name_no_hyphen)
    replace_svc_in_file('tests/test_service.py', service_name_no_hyphen)
    replace_svc_in_file('serve', service_name_no_hyphen)
    replace_svc_in_file('train', service_name_no_hyphen)
    replace_svc_in_file('setup.py', service_name_no_hyphen)
    replace_svc_in_file('MANIFEST.in', service_name_no_hyphen)

if __name__ == "__main__":
    main()
    