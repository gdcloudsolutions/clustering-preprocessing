# clustering-preprocessing

store clustering for assortment optimization

## Commands

Commands use a Makefile to simplify developer life. Current Makefile enable the following task :

 - `init`: Initialize your project

 - `install`: Install the dependencies according to the `setup.py` file
 
 - `model/train`: Train your model locally
 
 - `model/serve`: Serve your server to receive invocation request

 - `bumpversion`: Bmpversion if the working copy is clean else raise an error. Use `PART` parameter to bump the `major`, `minor` or `patch` part of the version. Default is `patch`.

 - `test`: This task call the subtask `lint` then `unittest`
 
 - `unittest`: Execute all unit tests on whole `src` folder
 
 - `lint`: Execute Pylint on whole `src` folder

 - `coverage`: Execute test and report the code coverage.

 - `docker/start`: This task call the subtasks `docker/build` then `docker/run`

 - `docker/build`: Build the docker file

 - `docker/run`: Run the docker container. This command should use `CMD` parameter to override default docker command during the run.

 - `encrypt`: Use google cloud command to encrypt a file. Use `FILE` parameter to set the file path.

 - `decrypt`: Use google cloud command to encrypt a file. Use `FILE` parameter to set the file path.

 - `cfg-omni`: Copy omni configuration to the project namespace. Use `ENV` parameter to set the environment.
 
 > Tip for `install` task
 > 
 > By default `install` task will use the `dev` profile and `dev` environment in PyPI.
 > If you want to use an other you can simply override this value when you call the task : `$ make ENV=prod`

### Clean the Scaffold

File you have to modify :

1) Launch `make init/service name="your-custom-service-name" desc="your description of service"`

2) Launch `make clean/scaffold` to remove hello world

> Instead of update version manually in `setup.py` and `.bumpversion.cfg`, you can use the command `$ make bumpversion`

## Configuration

Project will automatically use the file `config/default.json` and importe all the properties into `settings.py` module as a dictionnary.

It will also override the properties found in the default file with properties found in `config/local.json`. This file is ignore by git, so use it to override properties for your environment. You also could use this file to add properties specific to your environment.

For example:
The project need a configuration to get database's uri. So in `config/default.json` we can found something like that :

```json
{
    "database": {
        "uri": "DEFAULT_DATABASE_URI"
    }
}
```

But locally you need your own URI. So you could override this properties without change the default config file, but simply creating `local.json` file with this content :

```json
{
    "database": {
        "uri": "YOUR_OWN_DATABASE_URI"
    }
}
```

## Run with Docker

### Docker build

```bash
export VERSION=$(git log --pretty=format:'%h' -n 1)
export BRANCH_NAME=dev
export NAME=$(basename $(pwd))

docker build -t $NAME:$VERSION -t $NAME:latest --build-arg BRANCH_NAME=$BRANCH_NAME .
```

### Docker run

```bash
export NAME=$(basename $(pwd))

docker run -it -p 80:5000 $NAME:latest
```
