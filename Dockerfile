FROM python:3
EXPOSE 8080

ARG BRANCH_NAME
ENV BRANCH_NAME $BRANCH_NAME
ENV MODEL_PATH /opt/ml/model

WORKDIR /usr/src/app/

RUN mkdir -p /opt/ml/model
ENV PATH="/usr/src/app:${PATH}"

# Copy necessary files
COPY setup.py .
COPY README.md .

COPY clustering_preprocessing ao_store_clustering
COPY MANIFEST.in .

COPY train .
COPY serve .

RUN chmod +x train
RUN chmod +x serve

RUN pip install -i https://pypi.api.dynamite.ca/dynamite/public --no-cache-dir --compile .[prod]

# Clean up
RUN rm -rf ./ao_store_clustering setup.py README.md MANIFEST.in

CMD ["train", "serve"]
